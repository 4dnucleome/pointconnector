#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: zparteka
"""

import argparse
from operations.points_library import arrange_points
from operations.traveling_salesman import solve
from operations.neighbour_joining import njoin


def main():
    parser = argparse.ArgumentParser(description="description")
    parser.add_argument('-p', '--pdb', default="PDB file with unorganized points")
    parser.add_argument('-o', '--outpath', help='where to save your results', default='./')
    parser.add_argument('-m', '--method', default='nn', help="Different algorithms connecting points. Choose from 'nj'"
                                                             "- Neighbor Joining, 'tsp'-Traveling Salesman Solver, "
                                                             "'nn' - Nearest Neighbor ")
    args = parser.parse_args()
    if args.method == 'nn':
        arrange_points(pdb_file=args.pdb, outpath=args.outpath)
    elif args.method == 'tsp':
        solve(pdb_file=args.pdb, outpath=args.outpath)
    elif args.method == 'nj':
        njoin(infile=args.pdb, outpath=args.outpath)
    else:
        print('Wrong method')


if __name__ == '__main__':
    main()