#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 10:31:55 2017
@author: zparteka
"""

from __future__ import absolute_import, division, print_function
from skbio import DistanceMatrix
from skbio.tree import nj
from .points_library import distance_matrix
from .points_library import savePointsAsPdb, pointReader
from os import path


def njoin(infile, outpath):
    """Use Neighbour Joining to connect unorganised set of points."""
    distance = distance_matrix(infile)
    start_points = pointReader(infile)
    ids = []
    for i in range(len(distance)):
        ids.append(str(i))
    dm = DistanceMatrix(distance, ids)
    tree = nj(dm, result_constructor=str)
    tre1 = tree.replace("(", ",")[:-1]
    tre2 = tre1.replace(")", ",")
    tre3 = tre2.replace(":", ",").strip().split(",")
    number = 0
    ipath = []
    for j in tre3:
        if j != '' and j != ' ':
            if len(j) <= 4:
                number += 1
                ipath.append(int(j))
    points = [0] * len(ipath)
    for i in range(len(ipath)):
        points[i] = start_points[ipath[i]]
    outfile = path.join(outpath, path.basename(infile)[:-4] + '_NJ.pdb')
    savePointsAsPdb(points, outfile, connect=True)
