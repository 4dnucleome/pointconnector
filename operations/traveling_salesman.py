#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 11:57:02 2017

@author: zparteka
"""

from tsp_solver.greedy import solve_tsp
from .points_library import savePointsAsPdb, pointReader
from .points_library import distance_matrix
from os import path


def solve(pdb_file, outpath):
    """ Takes pdb file with points, and constructs 3d structure using traveling salesman algorithm"""
    start_points = pointReader(pdb_file)
    matrix = distance_matrix(pdb_file)
    apath = solve_tsp(matrix)
    points = [0] * len(apath)
    for i in range(len(apath)):
        points[i] = start_points[apath[i]]
    savePointsAsPdb(points, path.join(outpath, path.basename(pdb_file)[:-4] + '_TSP.pdb'), connect=True)
