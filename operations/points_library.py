#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: Zofia Parteka
import math
import os
import numpy as np
import re


def distance_matrix(pdb_file, save=None):
    """Read points from .PDB file and create distance matrix between those points."""
    if not pdb_file.endswith('.pdb'):
        return "Wrong file format"
    points = pointReader(pdb_file)
    n = len(points)
    distances = np.zeros((n, n))
    for i in range(len(points)):
        for j in range(len(points)):
            x1, y1, z1 = points[i][0], points[i][1], points[i][2]
            x2, y2, z2 = points[j][0], points[j][1], points[j][2]
            distance = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2 + (z2 - z1) ** 2)
            distances[i][j] = distance
            distances[j][i] = distance
    if save:
        np.savetxt(save, distances, fmt='%d')
        print("File {} saved...".format(save))
    else:
        return distances


def find_furthest(pdb_file):
    '''
    find two points that are furthest apart from each other
    :param dist_matr: matrix of distances
    :return: coordinates of two furthest points
    '''
    points = pointReader(pdb_file)
    dist_matr = distance_matrix(pdb_file)
    dist_max = np.nonzero(dist_matr == dist_matr.max())
    max_points = np.transpose(dist_max)
    pp = []
    for i in range(len(max_points)):
        first = points[max_points[i][0]]
        second = points[max_points[i][1]]
        pp.append((first, second))
    return pp[0]


def find_closest_point(point, pdb_file, excluded_points=None):
    """Find the closest neighbour of a given point"""
    min_dist = float('inf')
    indices = None
    points = pointReader(pdb_file)
    if excluded_points:
        for i in range(len(excluded_points)):
            points.remove(excluded_points[i])
    x1, y1, z1 = point[0], point[1], point[2]
    for i in range(len(points)):
        if points[i] != point:
            x2, y2, z2 = points[i][0], points[i][1], points[i][2]
            distance = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2 + (z2 - z1) ** 2)
            if distance < min_dist:
                min_dist = distance
                indices = points[i]
        else:
            pass
    return indices


def arrange_points(pdb_file, outpath=None):
    """Nearest Neighbour algorithm."""

    points = pointReader(pdb_file)
    if len(points) == 0:
        print("Points not found in file " + pdb_file)
        return
    res = find_furthest(pdb_file)
    for j in range(len(res)):
        start_point = res[j]
        points.remove(start_point)
        if start_point in points:
            points.remove(start_point)
        ordered_points = [start_point]
        excluded_points = [start_point]
        for i in range(len(points)):
            new_point = find_closest_point(start_point, pdb_file, excluded_points)
            start_point = new_point
            ordered_points.append(start_point)
            excluded_points.append(start_point)
        if outpath:
            name = os.path.basename(pdb_file)[:-4] + '_NN_' + str(j + 1) + '.pdb'
            filename = os.path.join(outpath, name)
            savePointsAsPdb(ordered_points, filename)
        else:
            filename = pdb_file[:-4] + '_NN_' + str(j + 1) + '.pdb'
            savePointsAsPdb(ordered_points, filename)

        print(filename + ' saved..')


def savePointsAsPdb(points, filename, verbose=True, connect=True):
    """Save points in .PDB format."""
    atoms = ''
    n = len(points)
    for i in range(n):
        x = points[i][0]
        y = points[i][1]
        try:
            z = points[i][2]
        except IndexError:
            z = 0.0
        atoms += (
            '{0:6}{1:>5}  {2:3}{3:}{4:3} {5:}{6:>4}{7:}   {8:8.3f}{9:8.3f}{10:8.3f}{11:6.2f}{12:6.2f}{13:>12}\n'.format(
                "ATOM", i + 1, 'B', ' ', 'BEA', 'A', i + 1, ' ', x, y, z, 0, 0, 'C'))

    connects = ''
    if connect:
        if n != 1:
            connects = 'CONECT    1    2\n'
            for i in range(2, n):
                connects += 'CONECT{:>5}{:>5}{:>5}\n'.format(i, i - 1, i + 1)
            connects += 'CONECT{:>5}{:>5}\n'.format(n, n - 1)

    pdbFileContent = atoms + connects
    open(filename, 'w').write(pdbFileContent)
    if verbose:
        print("File {} saved...".format(filename))
    return filename


def pointReader(fname):
    """Read points from a .PDB file."""
    atoms = [i.strip() for i in open(fname) if re.search('^(ATOM|HETATM)', i)]
    points = []
    for i in atoms:
        x = float(i[30:38])
        y = float(i[38:46])
        z = float(i[46:54])
        points.append((x, y, z))
    return points
