# pointConnector

This repository contains methods for connecting unorganised set of points in 3D space. Main function is **connect_points.py**. All methods are implemented in *operations* package.
Python 3.4 (or later) is required.

## Parameters:
* -p, --pdb - Set of points in .PDB format.
* -o, --outpath - Directory to save output. Bu default the files with connected points are saved where the script is located.
* -m, --method - Different algorithms used to create a path between all input points. Options available: nn (Nearest Neighbour), tsp (Travelling Salesman Problem), nj (Neighbour Joining). See below for description.

## Point connecting methods

* TSP - Travelling Salesman Problem solver - uses tsp_solver package (https://github.com/dmishin/tsp-solver) to find suboptimal shortest path visiting all input points. Creates globally shortest path.
* Neighbor Joining - uses scikit-bio (http://scikit-bio.org/) tree construction function to create output structure from points.
* Nearest Neighbor - It starts by finding two points that are the most distant from each other and builds two paths each starting from one of the points (it saves two structures). In each step the algorithm is looking for the nearest neighbor of current point. Its a greedy and local solution.


## Requirements

* Python 3.4 or later
* Scikit-bio
* Numpy
* tsp_solver